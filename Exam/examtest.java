package com.nexsoft.Exam;

public class examtest {
    public static void main(String[] args) {
        int Y = 2017;    // year
        int startDayOfMonth = 5;
        int spaces = 1;

        // months[i] = name of month i
        String[] months = {
                "",                               // leave empty so that we start with months[1] = "January"
                "Januari", "Februari", "Maret",
                "April", "Mei", "Juni",
                "Juli", "Agustus", "September",
                "Oktober", "November", "Desember"
        };

        // days[i] = number of days in month i
        int[] days = {
                0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
        };

        for (int M = 1; M <= 12; M++) {

            // check for leap year
            if  ((((Y % 4 == 0) && (Y % 100 != 0)) ||  (Y % 400 == 0)) && M == 2)
                days[M] = 29;


            // print calendar header
            // Display the month and year
            System.out.println("========Bulan "+ months[M] + " " + Y + "========");

            // Display the lines
            System.out.println("_____________________________________");
            System.out.println("   M    S    S    R    K    J    S");

            // spaces required
            spaces = (days[M-1] + spaces)%7;

            // print the calendar
            for (int i = 0; i < spaces; i++)
                System.out.print("     ");
            for (int i = 1; i <= days[M]; i++) {
                System.out.printf(" %3d ", i);
                if (((i + spaces) % 7 == 0) || (i == days[M])) System.out.println();
            }

            System.out.println();
        }
    }
    }

