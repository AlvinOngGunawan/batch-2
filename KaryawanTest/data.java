package com.nexsoft.KaryawanTest;

public class data extends Salary {
    data(int CardID, String InputIDName, int NewparkingID, long NewSalary, String NewJabatan){
        super.setCardNumber(CardID);
        super.setName(InputIDName);
        super.setparkingId(NewparkingID);
        super.setSalary(NewSalary);
        super.setJabatan(NewJabatan);
    }
    public void showData(){
        System.out.println("ID CARD PEGAWAI      = " + super.getCardNumber());
        System.out.println("Nama Pegawai         = " + super.getName());
        System.out.println("Nomor Parkir         = " + super.getParkingId());
        System.out.println("Gaji pegawai         = " + super.getSalary());
        System.out.println("Jabatan              = " + super.getJabatan());
    }
}
